Cara Pengunaan
==============

Get All Data Country
====================
GET : http://localhost/yii2advancedrestapi/backend/web/index.php?r=country/getcountry

Create Data Country
===================
POST : http://localhost/yii2advancedrestapi/backend/web/index.php?r=country/createcountry
parameter
code, name, population

Update Data Country
===================
POST : http://localhost/yii2advancedrestapi/backend/web/index.php?r=country/updatecountry
parameter
code, name, population

Delete Data Country
===================
POST : http://localhost/yii2advancedrestapi/backend/web/index.php?r=country/deletecountry
parameter
code
